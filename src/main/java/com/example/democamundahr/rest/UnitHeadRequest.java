package com.example.democamundahr.rest;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UnitHeadRequest {
    private int headId;
}
