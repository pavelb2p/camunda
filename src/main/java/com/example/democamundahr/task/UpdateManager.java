package com.example.democamundahr.task;


import com.example.democamundahr.rest.UnitHeadRequest;
import com.example.democamundahr.service.UnitService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;


@Component("UpdateManager")
public class UpdateManager implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) throws Exception {

        UnitHeadRequest unitHeadRequest = new UnitHeadRequest(
                Integer.parseInt((String) execution.getVariable("employeeId"))
        );

        UnitService.updateUnitHead(unitHeadRequest, (int) execution.getVariable("unit"));

    }
}
