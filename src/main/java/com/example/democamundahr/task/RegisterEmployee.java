package com.example.democamundahr.task;

import com.example.democamundahr.rest.EmployeeRequest;
import com.example.democamundahr.service.EmployeeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

@Component("RegisterEmployee")
public class RegisterEmployee implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) throws Exception {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

        EmployeeRequest employeeRequest = new EmployeeRequest(
                (String) execution.getVariable("familyName"),
                (String) execution.getVariable("firstName"),
                (String) execution.getVariable("secondName"),
                (int) execution.getVariable("unit"),
                (int) execution.getVariable("position"),
                (int) execution.getVariable("salary"),
                dateFormat.format(execution.getVariable("dateHire"))
        );
        String employeeId = EmployeeService.registerEmployee(employeeRequest);
        execution.setVariable("employeeId", employeeId);
    }

}
