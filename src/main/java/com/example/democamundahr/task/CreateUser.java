package com.example.democamundahr.task;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.identity.User;
import org.springframework.stereotype.Component;

@Component("CreateUser")
public class CreateUser implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        IdentityService identityService = execution.getProcessEngine().getIdentityService();
        User newUser = identityService.newUser((String) execution.getVariable("employeeId"));
        newUser.setLastName((String) execution.getVariable("familyName"));
        newUser.setFirstName((String) execution.getVariable("firstName"));
        newUser.setPassword("password");
        identityService.saveUser(newUser);
    }
}
